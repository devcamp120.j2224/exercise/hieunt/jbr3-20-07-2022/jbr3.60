package com.jbr360.accountrestapi;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class AccountController {
    @CrossOrigin
    @GetMapping("/accounts")
    public static ArrayList<Account> getAccountList() {
        Account account1 = new Account("1", "Lan", 3000);
        Account account2 = new Account("2", "Nam", 4000);
        Account account3 = new Account("3", "Hoa", 5000);
        System.out.println("account1 la: " + account1);
        System.out.println("account2 la: " + account2);
        System.out.println("account3 la: " + account3);
        ArrayList<Account> accountList = new ArrayList<Account>();
        accountList.add(account1);
        accountList.add(account2);
        accountList.add(account3);
        return accountList;
    }
}
